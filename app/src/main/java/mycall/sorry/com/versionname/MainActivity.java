package mycall.sorry.com.versionname;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class MainActivity extends Activity implements View.OnClickListener {
    private Button btn_clear,btn_ok;
    private EditText version_one,version_two;
    private String version_1,version_2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        btn_clear= (Button) findViewById(R.id.clear);
        btn_ok= (Button) findViewById(R.id.ok);
        version_one= (EditText) findViewById(R.id.versioncodeone);
        version_two= (EditText) findViewById(R.id.versioncodetwo);
        btn_clear.setOnClickListener(this);
        btn_ok.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.ok:
                version_1=version_one.getText().toString().trim();
                version_2=version_two.getText().toString().trim();
                if(version_1!=null && !"".equals(version_1)&&version_2!=null &&
                        !"".equals(version_2) && rule(version_1,version_2)){
                    VersionStringCompare(version_1,version_2);
                }
                else{
                    Toast.makeText(this,"版本号不符和标准,请检查后重试",Toast.LENGTH_LONG).show();
                }
                break;
            case R.id.clear:
                version_one.setText("");
                version_two.setText("");
                break;
        }
    }
    public void VersionStringCompare(String s1,String s2) {
        String[] one = s1.split("\\.");
        String[] two = s2.split("\\.");
        int nameSize= one.length < two.length?one.length:two.length;
        for (int i = 0; i < nameSize; i++) {
            if(!one[i].equals(two[i])){
                if (Integer.parseInt(one[i]) > Integer.parseInt(two[i])) {
                    version_one.setText("1 bigger");
                } else if (Integer.parseInt(one[i]) < Integer.parseInt(two[i])) {
                    version_one.setText("2 bigger");
                }
                return;
            }

        }
        if(one.length==two.length){
            version_one.setText("Like");
        }else if(one.length >two.length){
            version_one.setText("1 bigger");
        }else if(one.length < two.length){
            version_one.setText("2 bigger");
        }
    }
    public boolean rule(String s1,String s2){
        Pattern p = Pattern.compile("^([0-9]+\\.{0,1}[0-9]{0,2})*[0-9]$");
        Matcher m1 = p.matcher(s1);
        Matcher m2 = p.matcher(s2);
        return (m1.matches() && m2.matches());
    }
}
